package uz.pdp.audit.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.audit.payload.LoginDTO;
import uz.pdp.audit.payload.RegisterDTO;
import uz.pdp.audit.service.AuthService;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;

    @PostMapping("/register")
    public HttpEntity<?> signUp(@Valid @RequestBody RegisterDTO registerDTO) {
        boolean register = authService.register(registerDTO);
        return ResponseEntity.status(HttpStatus.CREATED.value()).body(register);
    }


    @PostMapping("/login")
    public HttpEntity<?> signIn(@Valid @RequestBody LoginDTO loginDTO) {
        String token = authService.login(loginDTO);
        return ResponseEntity.status(HttpStatus.OK.value()).body(token);
    }

}
