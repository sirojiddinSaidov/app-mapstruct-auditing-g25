package uz.pdp.audit.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.audit.entity.Post;
import uz.pdp.audit.mapper.PostMapper;
import uz.pdp.audit.payload.PostAddDTO;
import uz.pdp.audit.payload.PostDTO;
import uz.pdp.audit.repository.PostRepository;

import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/post")
public class PostController {

    private final PostRepository postRepository;
    private final PostMapper postMapper;

    @PostMapping
    public HttpEntity<PostDTO> add(@RequestBody PostAddDTO postAddDTO) {
        Post post = postMapper.toPost(postAddDTO);
        System.out.println("Before save");
        postRepository.save(post);
        System.out.println("After save");
        PostDTO postDTO = postMapper.toPostDTO(post);
        return ResponseEntity.status(HttpStatus.CREATED).body(postDTO);
    }

    @PutMapping("/{id}")
    public HttpEntity<?> edit(@PathVariable Long id,
                              @RequestBody PostAddDTO postAddDTO) {
        Optional<Post> optionalPost = postRepository.findById(id);
        if (optionalPost.isPresent()) {
            Post post = optionalPost.get();

            post.setText(postAddDTO.getText());
            post.setTitle(postAddDTO.getTitle());
            postMapper.updatePost(postAddDTO, post);

            postRepository.save(post);
        }
        return null;
    }
}
