package uz.pdp.audit.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.pdp.audit.entity.User;
import uz.pdp.audit.mapper.UserMapper;
import uz.pdp.audit.payload.LoginDTO;
import uz.pdp.audit.payload.RegisterDTO;
import uz.pdp.audit.repository.UserRepository;

import java.util.Date;

@Service
@RequiredArgsConstructor
public class AuthService {

    public static final String key = "7#70OZn0#Dsao8989DA1%T&hBnrzgqK28o";

    private final UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;


    public boolean register(RegisterDTO registerDTO) {
        if (userRepository.existsByEmail(registerDTO.getEmail()))
            throw new RuntimeException("User already exists");

        User user = userMapper.mapFromRegisterDTOToUser(registerDTO, passwordEncoder);
//        user.setPassword(passwordEncoder.encode(registerDTO.getPassword()));

        userRepository.save(user);
        return true;
    }

    public String login(LoginDTO loginDTO) {

        User user = userRepository.findByEmail(loginDTO.getEmail()).orElseThrow(() -> new RuntimeException("User not found"));

        if (!passwordEncoder.matches(loginDTO.getPassword(), user.getPassword()))
            throw new RuntimeException("User or password error");

        String token = Jwts.builder()
                .setSubject(String.valueOf(user.getId()))
                .setExpiration(new Date(System.currentTimeMillis() + 86_400_000))
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, key)
                .compact();

        return token;
    }
}
