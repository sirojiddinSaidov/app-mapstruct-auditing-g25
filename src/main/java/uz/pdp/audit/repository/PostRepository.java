package uz.pdp.audit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import uz.pdp.audit.entity.Post;

public interface PostRepository extends JpaRepository<Post, Long> {
}