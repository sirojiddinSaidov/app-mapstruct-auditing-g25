package uz.pdp.audit.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import uz.pdp.audit.entity.Post;
import uz.pdp.audit.payload.PostAddDTO;
import uz.pdp.audit.payload.PostDTO;

@Mapper(componentModel = "spring")
public interface PostMapper {
    Post toPost(PostAddDTO postAddDTO);

    PostDTO toPostDTO(Post post);

    @Mapping(target = "id", ignore = true)
    void updatePost(PostAddDTO postAddDTO, @MappingTarget Post post);
}
