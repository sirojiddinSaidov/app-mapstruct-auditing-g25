package uz.pdp.audit.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * A DTO for the {@link uz.pdp.audit.entity.Post} entity
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostAddDTO implements Serializable {

    private Long id;

    private String title;

    private String text;
}