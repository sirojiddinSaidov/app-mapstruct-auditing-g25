package uz.pdp.audit.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import uz.pdp.audit.entity.User;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * A DTO for the {@link uz.pdp.audit.entity.Post} entity
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class PostDTO implements Serializable {

    private Long id;

    private String title;

    private String text;

    private String url;

    private User createdBy;

    private Integer updatedById;

    private Timestamp createdAt;

    private Timestamp updatedAt;

}